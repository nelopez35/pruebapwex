<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('menu', 'MenuController@index');

//CUSTOMERS

Route::resource('customers', 'CustomersController')->middleware('auth');

Route::get('customers/{id}/delete', [
    'as' => 'customers.delete',
    'uses' => 'CustomersController@destroy',
])->middleware('auth');


//SELLERS



Route::resource('sellers', 'SellersController')->middleware('auth');;

Route::get('sellers/{id}/delete', [
    'as' => 'sellers.delete',
    'uses' => 'SellersController@destroy',
])->middleware('auth');


//CATALOG PRODUCTS

Route::resource('catalogs', 'CatalogController')->middleware('auth');;

Route::get('catalogs/{id}/delete', [
    'as' => 'catalogs.delete',
    'uses' => 'CatalogController@destroy',
])->middleware('auth');


//ORDERS


Route::resource('orders', 'OrdersController')->middleware('auth');;

Route::get('orders/{id}/delete', [
    'as' => 'orders.delete',
    'uses' => 'OrdersController@destroy',
])->middleware('auth');


//ORDERS by SELLER

Route::get('/order-seller', 'OrdersController@orderBySeller')->middleware('auth');

Route::post('/get-order-seller', 'OrdersController@getOrderBySeller')->middleware('auth');
