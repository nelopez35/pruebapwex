@extends('layouts.app')
<script src="{{ asset('js/app.js') }}"></script>
@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'orders.store']) !!}

        @include('orders.fields')

    {!! Form::close() !!}
</div>

@endsection
