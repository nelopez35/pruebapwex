@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Orders</h1>
            <a class="btn btn-primary pull-right" style="    margin-top: 8px; margin-left: 10px; margin-bottom: 20px;" href="{!! route('orders.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($orders->isEmpty())
                <div class="well text-center">No Orders found.</div>
            @else
                <table class="table">
                    <thead>
                    <th width="50px">ID</th>
                    <th width="50px">Customer</th>
                    <th width="50px">Products</th>
                    <th width="50px">Total</th>
                    </thead>
                    <tbody>
                     
                    @foreach($orders as $orders)
                        <tr>
                            <td>{!! $orders->id !!}</td>
                            <td>{!! $orders->name !!}</td>
                            <td>{!! $orders->ids !!}</td>
                            <td>{!! $orders->total !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection