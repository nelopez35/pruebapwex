@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Orders by Seller</h1>
        </div>
        <div class="form-group col-sm-6 col-lg-4">
            {!! Form::label('seller', 'Seller:') !!}
            <select  name="seller_id" id="seller" class="form-control">
            @foreach($sellers as $aKey => $aSport)
                <option value="{{$aKey}}">{{$aSport}}</option>
            @endforeach
            </select>
            <br>
            <button class="btn btn-primary search">Search</button>
        </div>

        <div class="row">
            
            <div class="form-group col-sm-12 col-lg-12">

            </div>

            <div class="table-responsive">
                <div class="table-responsive">
                <table class="table table-bordered orders">
                  <thead>
                    <tr>
                      <th># Order</th>
                      <th>Product</th>
                      <th>Qty</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
            </div>
        </div>
    <script type="text/javascript">
    
        (function($){

            $(document).ready(function () {
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $(".search").click(function(e){
                    e.preventDefault();

                    $('.orders').find('tbody').empty();

                    var seller = $("#seller").val();

                    $.ajax({

                       type:'POST',

                       url:'/get-order-seller',

                       data:{seller:seller},

                       success:function(data){

                            if (typeof data != 'undefined') {

                                var tbody = '';

                                $.each(data, function( index, value ) {

                                    tbody +=  "<tr> <td class='td col1'>" + value.id +" </td> \
                                            <td class='td col2'>" + value.name +"</td> \
                                            <td class='td col3'>" + value.qty +"</div></td> \
                                        </tr>";

                                });

                                $('.orders').find('tbody').append(tbody);
                            }

                       }

                    });
                });
            });
            
        })(jQuery); 
        

    </script>
@endsection