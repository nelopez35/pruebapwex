<!-- customers field -->

<div class="form-group col-sm-12">
  	{!! Form::label('customers', 'Customer:') !!}
  	<select  name="customer" id="customers">
	@foreach($customers as $aKey => $aSport)
	    <option value="{{$aKey}}">{{$aSport}}</option>
	@endforeach
	</select>
</div>

<!-- catalog field -->

<div class="form-group col-sm-12">
  	{!! Form::label('products', 'Products:') !!}
  	<select  name="" id="products">
	@foreach($products as $aKey => $aSport)
	    <option value="{{$aKey}}">{{$aSport}}</option>
	@endforeach
	</select>
</div>

<table id="product-list" class=" table order-list">
    <thead>
        <tr>
            <td>Name</td>
            <td>Qty</td>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
  
</table>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>


<script>
  $(function() {

  	var counter = 0;

  	$('#customers').multipleSelect({
      filter: true
     });

    $('#products').multipleSelect({
      filter: true,
      onClick: function (element){
  		var newRow = $("<tr>");
        var cols = "";
        console.log(element);
        cols += '<td>'+element.text+'</td>';
        cols += '<input type="hidden" class="product-'+element.value+'" value="'+element.value+'" name="products[]"/>';
        cols += '<td><input type="text" name="qty[]" class="form-control"/></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger " value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;

      }
    });

    $(document).ready(function () {
	   
	    $("table.order-list").on("click", ".ibtnDel", function (event) {
	        $(this).closest("tr").remove();       
	        counter -= 1
	    });


	});



	function calculateRow(row) {
	    var price = +row.find('input[name^="price"]').val();

	}

	function calculateGrandTotal() {
	    var grandTotal = 0;
	    $("table.order-list").find('input[name^="price"]').each(function () {
	        grandTotal += +$(this).val();
	    });
	    $("#grandtotal").text(grandTotal.toFixed(2));
	}
  })
</script>