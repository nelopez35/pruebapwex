@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($customers, ['route' => ['customers.update', $customers->id], 'method' => 'PATCH']) !!}

        @include('customers.fields')

    {!! Form::close() !!}
</div>
@endsection
