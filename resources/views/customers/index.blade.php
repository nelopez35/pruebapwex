@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Customers</h1>
            <a class="btn btn-primary pull-right" style="    margin-top: 8px; margin-left: 10px; margin-bottom: 20px;" href="{!! route('customers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($customers->isEmpty())
                <div class="well text-center">No Customers found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Name</th>
			<th>Lastname</th>
			<th>Email</th>
			<th>Phone</th>
			<th>Address</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($customers as $customers)
                        <tr>
                            <td>{!! $customers->name !!}</td>
					<td>{!! $customers->lastname !!}</td>
					<td>{!! $customers->email !!}</td>
					<td>{!! $customers->phone !!}</td>
					<td>{!! $customers->address !!}</td>
                            <td>
                                <a href="{!! route('customers.edit', [$customers->id]) !!}"><i class="glyphicon glyphicon-edit"></i>edit</a>
                                <a href="{!! route('customers.delete', [$customers->id]) !!}" onclick="return confirm('Are you sure wants to delete this Customers?')"><i class="glyphicon glyphicon-remove"></i>delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection