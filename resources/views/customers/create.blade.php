@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'customers.store']) !!}

        @include('customers.fields')

    {!! Form::close() !!}
</div>
@endsection
