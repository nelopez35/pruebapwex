@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($catalog, ['route' => ['catalogs.update', $catalog->id], 'method' => 'patch']) !!}

        @include('catalogs.fields')

    {!! Form::close() !!}
</div>
@endsection
