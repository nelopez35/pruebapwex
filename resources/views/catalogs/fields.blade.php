<!--- Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!--- Qty Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('qty', 'Qty:') !!}
    {!! Form::text('qty', null, ['class' => 'form-control']) !!}
</div>

<!--- Width Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('width', 'Width:') !!}
    {!! Form::text('width', null, ['class' => 'form-control']) !!}
</div>

<!--- Height Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('height', 'Height:') !!}
    {!! Form::text('height', null, ['class' => 'form-control']) !!}
</div>

<!--- Color Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('color', 'Color:') !!}
    {!! Form::text('color', null, ['class' => 'form-control']) !!}
</div>

<!--- Price Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- seller field -->

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('seller', 'Seller:') !!}
    <select  name="seller_id" id="seller" class="form-control">
    @foreach($sellers as $aKey => $aSport)
        <option value="{{$aKey}}">{{$aSport}}</option>
    @endforeach
    </select>
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
