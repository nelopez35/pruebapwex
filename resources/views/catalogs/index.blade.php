@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Catalogs</h1>
            <a class="btn btn-primary pull-right" style="    margin-top: 8px; margin-left: 10px; margin-bottom: 20px;" href="{!! route('catalogs.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($catalogs->isEmpty())
                <div class="well text-center">No Catalogs found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Name</th>
			<th>Qty</th>
			<th>Width</th>
			<th>Height</th>
			<th>Color</th>
            <th>Price</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($catalogs as $catalog)
                        <tr>
                            <td>{!! $catalog->name !!}</td>
					<td>{!! $catalog->qty !!}</td>
					<td>{!! $catalog->width !!}</td>
					<td>{!! $catalog->height !!}</td>
					<td>{!! $catalog->color !!}</td>
                    <td>{!! $catalog->price !!}</td>
                            <td>
                                <a href="{!! route('catalogs.edit', [$catalog->id]) !!}"><i class="glyphicon glyphicon-edit"></i>edit</a>
                                <a href="{!! route('catalogs.delete', [$catalog->id]) !!}" onclick="return confirm('Are you sure wants to delete this Catalog?')"><i class="glyphicon glyphicon-remove"></i>delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection