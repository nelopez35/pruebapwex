@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'catalogs.store']) !!}

        @include('catalogs.fields')

    {!! Form::close() !!}
</div>
@endsection
