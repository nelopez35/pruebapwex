@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($sellers, ['route' => ['sellers.update', $sellers->id], 'method' => 'patch']) !!}

        @include('sellers.fields')

    {!! Form::close() !!}
</div>
@endsection
