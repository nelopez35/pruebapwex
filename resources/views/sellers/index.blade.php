@extends('layouts.app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Sellers</h1>
            <a class="btn btn-primary pull-right" style="    margin-top: 8px; margin-left: 10px; margin-bottom: 20px;" href="{!! route('sellers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($sellers->isEmpty())
                <div class="well text-center">No Sellers found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Name</th>
			<th>Phone</th>
			<th>Address</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($sellers as $sellers)
                        <tr>
                            <td>{!! $sellers->name !!}</td>
					<td>{!! $sellers->phone !!}</td>
					<td>{!! $sellers->address !!}</td>
                            <td>
                                <a href="{!! route('sellers.edit', [$sellers->id]) !!}"><i class="glyphicon glyphicon-edit"></i>edit</a>
                                <a href="{!! route('sellers.delete', [$sellers->id]) !!}" onclick="return confirm('Are you sure wants to delete this Sellers?')"><i class="glyphicon glyphicon-remove"></i>delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection