@extends('layouts.app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'sellers.store']) !!}

        @include('sellers.fields')

    {!! Form::close() !!}
</div>
@endsection
