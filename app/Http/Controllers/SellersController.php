<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Libraries\Repositories\SellersRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class SellersController extends AppBaseController
{

	/** @var  SellersRepository */
	private $sellersRepository;

	function __construct(SellersRepository $sellersRepo)
	{
		$this->sellersRepository = $sellersRepo;
	}

	/**
	 * Display a listing of the Sellers.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->sellersRepository->search($input);

		$sellers = $result[0];

		$attributes = $result[1];

		return view('sellers.index')
		    ->with('sellers', $sellers)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Sellers.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sellers.create');
	}

	/**
	 * Store a newly created Sellers in storage.
	 *
	 * @param CreateSellersRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$request->validate([
		    "name" => "required",
			"phone" => "required",
			"address" => "required"
		]);
		 
        $input = $request->all();

		$sellers = $this->sellersRepository->store($input);

		Flash::message('Sellers saved successfully.');

		return redirect(route('sellers.index'));
	}

	/**
	 * Display the specified Sellers.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$sellers = $this->sellersRepository->findSellersById($id);

		if(empty($sellers))
		{
			Flash::error('Sellers not found');
			return redirect(route('sellers.index'));
		}

		return view('sellers.show')->with('sellers', $sellers);
	}

	/**
	 * Show the form for editing the specified Sellers.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$sellers = $this->sellersRepository->findSellersById($id);

		if(empty($sellers))
		{
			Flash::error('Sellers not found');
			return redirect(route('sellers.index'));
		}

		return view('sellers.edit')->with('sellers', $sellers);
	}

	/**
	 * Update the specified Sellers in storage.
	 *
	 * @param  int    $id
	 * @param CreateSellersRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$sellers = $this->sellersRepository->findSellersById($id);

		if(empty($sellers))
		{
			Flash::error('Sellers not found');
			return redirect(route('sellers.index'));
		}

		$sellers = $this->sellersRepository->update($sellers, $request->all());

		Flash::message('Sellers updated successfully.');

		return redirect(route('sellers.index'));
	}

	/**
	 * Remove the specified Sellers from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$sellers = $this->sellersRepository->findSellersById($id);

		if(empty($sellers))
		{
			Flash::error('Sellers not found');
			return redirect(route('sellers.index'));
		}

		$sellers->delete();

		Flash::message('Sellers deleted successfully.');

		return redirect(route('sellers.index'));
	}

}
