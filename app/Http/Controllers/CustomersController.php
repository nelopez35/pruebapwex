<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Libraries\Repositories\CustomersRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class CustomersController extends AppBaseController
{

	/** @var  CustomersRepository */
	private $customersRepository;

	function __construct(CustomersRepository $customersRepo)
	{
		$this->customersRepository = $customersRepo;
	}

	/**
	 * Display a listing of the Customers.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->customersRepository->search($input);

		$customers = $result[0];

		$attributes = $result[1];

		return view('customers.index')
		    ->with('customers', $customers)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Customers.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('customers.create');
	}

	/**
	 * Store a newly created Customers in storage.
	 *
	 * @param CreateCustomersRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{

		$request->validate([
			    "name" => "required",
				"lastname" => "required",
				"email" => "required|unique:customers",
				"phone" => "required",
				"address" => "required"
			]);

		$input = $request->all();

		$customers = $this->customersRepository->store($input);

		Flash::message('Customers saved successfully.');

		return redirect(route('customers.index'));
	}

	/**
	 * Display the specified Customers.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$customers = $this->customersRepository->findCustomersById($id);

		if(empty($customers))
		{
			Flash::error('Customers not found');
			return redirect(route('customers.index'));
		}

		return view('customers.show')->with('customers', $customers);
	}

	/**
	 * Show the form for editing the specified Customers.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$customers = $this->customersRepository->findCustomersById($id);

		if(empty($customers))
		{
			Flash::error('Customers not found');
			return redirect(route('customers.index'));
		}

		return view('customers.edit')->with('customers', $customers);
	}

	/**
	 * Update the specified Customers in storage.
	 *
	 * @param  int    $id
	 * @param CreateCustomersRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$customers = $this->customersRepository->findCustomersById($id);

		if(empty($customers))
		{
			Flash::error('Customers not found');
			return redirect(route('customers.index'));
		}

		$customers = $this->customersRepository->update($customers, $request->all());

		Flash::message('Customers updated successfully.');

		return redirect(route('customers.index'));
	}

	/**
	 * Remove the specified Customers from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$customers = $this->customersRepository->findCustomersById($id);

		if(empty($customers))
		{
			Flash::error('Customers not found');
			return redirect(route('customers.index'));
		}

		$customers->delete();

		Flash::message('Customers deleted successfully.');

		return redirect(route('customers.index'));
	}

}
