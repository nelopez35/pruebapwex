<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Sellers;
use App\Libraries\Repositories\CatalogRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class CatalogController extends AppBaseController
{

	/** @var  CatalogRepository */
	private $catalogRepository;

	function __construct(CatalogRepository $catalogRepo)
	{
		$this->catalogRepository = $catalogRepo;
	}

	/**
	 * Display a listing of the Catalog.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->catalogRepository->search($input);

		$catalogs = $result[0];

		$attributes = $result[1];


		return view('catalogs.index')
		    ->with('catalogs', $catalogs)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Catalog.
	 *
	 * @return Response
	 */
	public function create()
	{

		$sellers = Sellers::orderBy('name')->pluck('name', 'id');

		return view('catalogs.create') ->with('sellers',$sellers);
	}

	/**
	 * Store a newly created Catalog in storage.
	 *
	 * @param CreateCatalogRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $input = $request->all();

        $request->validate([
			    "name" => "required",
				"qty" => "required",
				"width" => "required",
				"height" => "required",
				"color" => "required",
				"price" => "required"
			]);

		$catalog = $this->catalogRepository->store($input);

		Flash::message('Catalog saved successfully.');

		return redirect(route('catalogs.index'));
	}

	/**
	 * Display the specified Catalog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$catalog = $this->catalogRepository->findCatalogById($id);

		if(empty($catalog))
		{
			Flash::error('Catalog not found');
			return redirect(route('catalogs.index'));
		}

		return view('catalogs.show')->with('catalog', $catalog);
	}

	/**
	 * Show the form for editing the specified Catalog.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$catalog = $this->catalogRepository->findCatalogById($id);

		if(empty($catalog))
		{
			Flash::error('Catalog not found');
			return redirect(route('catalogs.index'));
		}

		return view('catalogs.edit')->with('catalog', $catalog);
	}

	/**
	 * Update the specified Catalog in storage.
	 *
	 * @param  int    $id
	 * @param CreateCatalogRequest $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$catalog = $this->catalogRepository->findCatalogById($id);

		if(empty($catalog))
		{
			Flash::error('Catalog not found');
			return redirect(route('catalogs.index'));
		}

		$catalog = $this->catalogRepository->update($catalog, $request->all());

		Flash::message('Catalog updated successfully.');

		return redirect(route('catalogs.index'));
	}

	/**
	 * Remove the specified Catalog from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$catalog = $this->catalogRepository->findCatalogById($id);

		if(empty($catalog))
		{
			Flash::error('Catalog not found');
			return redirect(route('catalogs.index'));
		}

		$catalog->delete();

		Flash::message('Catalog deleted successfully.');

		return redirect(route('catalogs.index'));
	}

}
