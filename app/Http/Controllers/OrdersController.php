<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Orders;
use App\Models\Catalog;
use App\Models\Sellers;
use App\Models\Customers;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;
use Schema;
use DB;

class OrdersController extends AppBaseController
{

	/**
	 * Display a listing of the Post.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$query = Orders::query();
        $columns = Schema::getColumnListing('$TABLE_NAME$');
        $attributes = array();

        foreach($columns as $attribute){
            if($request[$attribute] == true)
            {
                $query->where($attribute, $request[$attribute]);
                $attributes[$attribute] =  $request[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        $orders = $query->get();

        $data = DB::table("orders")
          ->select("*",
                    DB::raw("(SELECT GROUP_CONCAT(catalogs.name) FROM order_items join catalogs on catalogs.id = order_items.product_id
                                WHERE order_items.order_id = orders.id
                                GROUP BY order_items.order_id) as ids"))
                    
           ->join('customers', 'customers.id', '=', 'orders.customer_id')->get();

        return view('orders.index')
            ->with('orders', $data)
            ->with('attributes', $attributes);
	}

	/**
	 * Show the form for creating a new Orders.
	 *
	 * @return Response
	 */
	public function create()
	{
		$catalog = Catalog::where('qty','>', 0)->orderBy('name')->pluck('name', 'id');

		$customers = Customers::orderBy('name')->pluck('name', 'id');

		return view('orders.create')->with('products',$catalog)->with('customers', $customers);
	}

	/**
	 * Store a newly created Orders in storage.
	 *
	 * @param CreateOrdersRequest $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $input = $request->all();

        $request->validate([
		    "customer" => "required",
			"products" => "required",
			"qty" => "required"
		]);
        
        $total = 0;

        $orders = new Orders;

		$orders->customer_id = $input['customer'];

		$orders->save();

		foreach ($input['products'] as $key => $value) {

			$orderItem = new OrderItems;

			$orderItem->product_id = $value;

			$orderItem->qty = $input['qty'][$key];

			$orderItem->order_id = $orders->id;

			$orderItem->save();
			
			$product = Catalog::find($value);

			$total += $product->price;
		}

		$orders->total = $total;

		$orders->save();


		Flash::message('Orders saved successfully.');

		return redirect(route('orders.index'));
	}

	/**
	 * Display the specified Orders.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$orders = Orders::find($id);

		if(empty($orders))
		{
			Flash::error('Orders not found');
			return redirect(route('orders.index'));
		}

		return view('orders.show')->with('orders', $orders);
	}

	/**
	 * Show the form for editing the specified Orders.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$orders = Orders::find($id);

		if(empty($orders))
		{
			Flash::error('Orders not found');
			return redirect(route('orders.index'));
		}

		return view('orders.edit')->with('orders', $orders);
	}

	/**
	 * Update the specified Orders in storage.
	 *
	 * @param  int    $id
	 * @param CreateOrdersRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateOrdersRequest $request)
	{
		/** @var Orders $orders */
		$orders = Orders::find($id);

		if(empty($orders))
		{
			Flash::error('Orders not found');
			return redirect(route('orders.index'));
		}

		$orders->fill($request->all());
		$orders->save();

		Flash::message('Orders updated successfully.');

		return redirect(route('orders.index'));
	}

	/**
	 * Remove the specified Orders from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Orders $orders */
		$orders = Orders::find($id);

		if(empty($orders))
		{
			Flash::error('Orders not found');
			return redirect(route('orders.index'));
		}

		$orders->delete();

		Flash::message('Orders deleted successfully.');

		return redirect(route('orders.index'));
	}

	public function orderBySeller(){

		$sellers = Sellers::orderBy('name')->pluck('name', 'id');

		return view('orders.orderbyseller')->with('sellers',$sellers);
	}

	public function getOrderBySeller(Request $request){

		$data = $request->all();

		$sellerId = $data['seller'];

		$data = DB::table("orders")
          	->select("orders.id","catalogs.name","order_items.qty")
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('catalogs', 'catalogs.id', '=', 'order_items.product_id')
            ->join('sellers', 'catalogs.seller_id', '=', 'sellers.id')
            ->where('sellers.id',$sellerId)->get();

        return response()->json($data);


	}
}
