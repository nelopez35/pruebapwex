<?php

namespace App\Libraries\Repositories;


use App\Models\Sellers;
use Illuminate\Support\Facades\Schema;

class SellersRepository
{

	/**
	 * Returns all Sellers
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Sellers::all();
	}

	public function search($input)
    {
        $query = Sellers::query();

        $columns = Schema::getColumnListing('sellers');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Sellers into database
	 *
	 * @param array $input
	 *
	 * @return Sellers
	 */
	public function store($input)
	{
		return Sellers::create($input);
	}

	/**
	 * Find Sellers by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Sellers
	 */
	public function findSellersById($id)
	{
		return Sellers::find($id);
	}

	/**
	 * Updates Sellers into database
	 *
	 * @param Sellers $sellers
	 * @param array $input
	 *
	 * @return Sellers
	 */
	public function update($sellers, $input)
	{
		$sellers->fill($input);
		$sellers->save();

		return $sellers;
	}
}