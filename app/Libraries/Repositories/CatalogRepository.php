<?php

namespace App\Libraries\Repositories;


use App\Models\Catalog;
use Illuminate\Support\Facades\Schema;

class CatalogRepository
{

	/**
	 * Returns all Catalogs
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Catalog::all();
	}

	public function search($input)
    {
        $query = Catalog::query();

        $columns = Schema::getColumnListing('catalogs');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Catalog into database
	 *
	 * @param array $input
	 *
	 * @return Catalog
	 */
	public function store($input)
	{
		return Catalog::create($input);
	}

	/**
	 * Find Catalog by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Catalog
	 */
	public function findCatalogById($id)
	{
		return Catalog::find($id);
	}

	/**
	 * Updates Catalog into database
	 *
	 * @param Catalog $catalog
	 * @param array $input
	 *
	 * @return Catalog
	 */
	public function update($catalog, $input)
	{
		$catalog->fill($input);
		$catalog->save();

		return $catalog;
	}
}