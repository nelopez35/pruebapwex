<?php

namespace App\Libraries\Repositories;


use App\Models\Customers;
use Illuminate\Support\Facades\Schema;

class CustomersRepository
{

	/**
	 * Returns all Customers
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Customers::all();
	}

	public function search($input)
    {
        $query = Customers::query();

        $columns = Schema::getColumnListing('customers');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Customers into database
	 *
	 * @param array $input
	 *
	 * @return Customers
	 */
	public function store($input)
	{
		return Customers::create($input);
	}

	/**
	 * Find Customers by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Customers
	 */
	public function findCustomersById($id)
	{
		return Customers::find($id);
	}

	/**
	 * Updates Customers into database
	 *
	 * @param Customers $customers
	 * @param array $input
	 *
	 * @return Customers
	 */
	public function update($customers, $input)
	{
		$customers->fill($input);
		$customers->save();

		return $customers;
	}
}