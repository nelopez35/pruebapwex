<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    
	public $table = "order_items";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "product_id",
	    "qty",
	    "order_id"
	];

	public static $rules = [
	    
	];

}
