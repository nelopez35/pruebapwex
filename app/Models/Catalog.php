<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    
	public $table = "catalogs";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "name",
		"qty",
		"width",
		"height",
		"color",
		"price",
		"seller_id"
	];

	public static $rules = [
	    "name" => "required",
		"qty" => "required",
		"width" => "required",
		"height" => "required",
		"color" => "required"
	];

}
