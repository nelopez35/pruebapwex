<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    
	public $table = "orders";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    
	];

	public static $rules = [
	    
	];
}
