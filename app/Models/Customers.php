<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    
	public $table = "customers";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "name",
		"lastname",
		"email",
		"phone",
		"address"
	];

	public static $rules = [
	    "name" => "required",
		"lastname" => "required",
		"email" => "required|unique:customers",
		"phone" => "required",
		"address" => "required"
	];

}
