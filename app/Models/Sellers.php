<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sellers extends Model
{
    
	public $table = "sellers";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "name",
		"phone",
		"address"
	];

	public static $rules = [
	    "name" => "required",
		"phone" => "required",
		"address" => "required"
	];

}
