<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('total')->nullable();
            $table->integer('customer_id');
            //$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->timestamps();

            
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
